<!DOCTYPE html>
<!-- saved from url=(0032)https://www.applyboard.com/login -->
<html data-original-title="" title=""><head data-original-title="" title=""><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
<meta http-equiv="X-UA-Compatible" content="IE=edge" data-original-title="" title="">
<link rel="shortcut icon" type="image/x-icon" href="https://d2mqj6445k6q7s.cloudfront.net/assets/favicon-f57bfd585212feb9ee8af498d79e96bc.ico" data-original-title="" title="">
<title data-original-title="" title="">
  Login
 | ApplyBoard</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0" data-original-title="" title="">

  <meta name="description" content="Log in to ApplyBoard to start your application now" data-original-title="" title="">

<!-- Google Tag Manager -->




<!-- Include style per-controller - vendor plugins -->

<!-- Main css styles -->
<link rel="stylesheet" media="all" href="login_src/application-3a1862be8520e5f5d7822a29ec664074.css" data-original-title="" title="">
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">

<!-- Include javascript per-controller - vendor plugins -->

<!-- Main javascript files -->
<script src="./Login _ ApplyBoard_files/application-db8842935958b93dbe2c3deeecd20b5a.js.download" data-original-title="" title=""></script>
<meta name="csrf-param" content="authenticity_token" data-original-title="" title="">
<meta name="csrf-token" content="Y9fEf1tla6Chsm84/lMa7rXU8aysfmty857dnlpEzn35XVGwvC7Yt0h9tpBpoupTnUQPULriRdrRN9tbfc+44w==" data-original-title="" title="">




<meta property="fb:app_id" content="689287211180493" data-original-title="" title="">
<meta property="og:url" content="https://www.applyboard.com/login" data-original-title="" title="">
<meta property="og:description" content="
  Login
 | ApplyBoard" data-original-title="" title="">


<link href="./Login _ ApplyBoard_files/survey.css" type="text/css" rel="stylesheet" data-original-title="" title="">






<script type="text/javascript" async="" src="./Login _ ApplyBoard_files/agile-webrules-min.js.download"></script></head>

<body class="top-navigation landing-page  mini-navbar fixed-nav  pace-done" data-original-title="" title=""><div class="pace  pace-inactive" data-original-title="" title=""><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);" data-original-title="" title="">
  <div class="pace-progress-inner" data-original-title="" title=""></div>
</div>
<div class="pace-activity" data-original-title="" title=""></div></div>
  <!-- Google Tag Manager (noscript) -->
<noscript data-original-title="" title=""><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NG4TVX3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Skin configuration box -->

  <div class="navbar-wrapper" data-original-title="" title="">
  <nav class="navbar navbar-default navbar-fixed-top navbar-scroll" role="navigation" data-original-title="" title="">
    <button id="nav-button" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" data-original-title="" title="">
      <span class="sr-only" data-original-title="" title="">Toggle navigation</span>
      <span class="icon-bar" data-original-title="" title=""></span>
      <span class="icon-bar" data-original-title="" title=""></span>
      <span class="icon-bar" data-original-title="" title=""></span>
    </button>

      
<div class="countdown-holder  hidden" data-original-title="" title="">
  <div class="countdown-open-button fa fa-bullhorn fa-lg" data-original-title="" title=""> </div>
  <div class="countdown-close-button fa fa-angle-double-right fa-lg" data-original-title="" title=""> </div>
  
</div>




    <div class="container" data-original-title="" title="">
      <div class="navbar-header page-scroll" id="navbar_ab_logo" style="position: absolute" data-original-title="" title="">
        <a href="https://www.applyboard.com/ca/" data-original-title="" title="">
          <img class="navbar-logo" src="./Login _ ApplyBoard_files/applyboard-9d0295fa6175b218d578c44aab49612c.png" alt="Applyboard" data-original-title="" title="">
</a>      </div>
      <div id="navbar" class="navbar-collapse collapse" data-original-title="" title="">
        <ul class="nav navbar-nav navbar-right" data-original-title="" title="">
          <li data-original-title="" title=""><a class="page-scroll" href="https://www.applyboard.com/schools" data-original-title="" title="">For Schools</a></li>
          <li data-original-title="" title=""><a class="page-scroll" href="https://www.applyboard.com/ca/faq_students" data-original-title="" title="">For Students</a></li>
          <li data-original-title="" title=""><a class="page-scroll" href="https://www.applyboard.com/ca/partner_with_us" data-original-title="" title="">For Recruiters</a></li>
            <li data-original-title="" title=""><a class="page-scroll _signup-btn" href="https://www.applyboard.com/account" data-original-title="" title="">My Account</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>


<!-- Wrapper-->
<div id="wrapper" class="devise_sessions-new" data-original-title="" title="">


  <!-- Navigation -->
  

  <!-- Page wraper -->
  <div id="page-wrapper" class="gray-bg " style="padding-top: 5px; min-height: 517px;" data-original-title="" title="">
    <div id="page_content" data-original-title="" title="">
      <!-- Page wrapper -->
      <!-- Main view  -->
      

<input name="utf8" type="hidden" value="✓" data-original-title="" title=""><input type="hidden" name="authenticity_token" value="Y9fEf1tla6Chsm84/lMa7rXU8aysfmty857dnlpEzn35XVGwvC7Yt0h9tpBpoupTnUQPULriRdrRN9tbfc+44w==" data-original-title="" title="">
  <div class="wrapper wrapper-content" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.05) 0%,rgba(0,0,0,0.1) 100%), url(https://d2mqj6445k6q7s.cloudfront.net/assets/login_register_footer-74071edb6662d77532a8b6bd63830985.png); background-size: cover;" data-original-title="" title="">
    <div class="container" data-original-title="" title="">
      <div class="row" data-original-title="" title="">
        <div class="col-md-12" data-original-title="" title="">
          <div class="ibox col-md-7 col-md-offset-3" data-original-title="" title="">
            <div class="ibox-title text-center" data-original-title="" title="">
              <h2 data-original-title="" title="">
                Login
              </h2>
            </div>
            <div class="ibox-content" data-original-title="" title="">

              <div style="padding: 20px 0px;" data-original-title="" title="">

            <a class="btn btn-block btn-social btn-google-plus" href="{{ url('/auth/google') }}" data-original-title="" title="">
    <i class="fa fa-google" data-original-title="" title=""></i> Sign in with Google
</a></div>
<div style="height: 1px; background-color: #ddd; text-align: center;margin:40px 0px" data-original-title="" title="">
  <div style="width:36px;height:36px;line-height:37px;margin:auto; border-radius:100%;border:1px solid #ddd;background-color: #f5f5f5; position: relative; top: -1.4em;color:#bbb;font-size:9pt" data-original-title="" title="">
    OR
  </div>
</div>
        

        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

            <div class="form-group" data-original-title="" title="">
                <label style="font-size: 14px;" data-original-title="" title="">Email:</label>
                <input autofocus="autofocus" autocomplete="email" class="form-control" type="email" name="email">
            </div>
            <div class="form-group" data-original-title="" title="">
                <label style="font-size: 14px;" data-original-title="" title="">Password:</label>
                <input autocomplete="current-password" class="form-control" type="password" name="password"></p>
            </div>
            <div class="field" data-original-title="" title="">
              <input type="hidden" name="redirect_to" id="redirect_to" data-original-title="" title="">
              <input name="user[remember_me]" type="hidden" value="0" data-original-title="" title=""><input type="checkbox" value="1" name="user[remember_me]" id="user_remember_me" data-original-title="" title="">
              <label for="user_remember_me" data-original-title="" title="">Remember me</label>
            </div>
            <div style="align-items: center; display: flex; justify-content: space-between;" data-original-title="" title="">
             
              <button type="submit" class="btn _login-register-btn">
                                    {{ __('Login') }}
                                </button>
            </div>
            <div style="text-align: center" data-original-title="" title="">
              <a class="forgot-password" href="https://www.applyboard.com/forgot_password" data-original-title="" title="">Forgot password?</a>
            </div>
         

         </form>



          <br data-original-title="" title="">
          <div style="text-align: center; margin-top: 15px; margin-bottom: 15px; font-size: 14px; font-weight: bold;" data-original-title="" title="">
            Don't have an account?
            <!-- -->
          </div>
          <div class="_register-disclaimer" data-original-title="" title="">
            <div class="_register-wrapper" data-original-title="" title="">
              <div class="_register-title" data-original-title="" title="">Student</div>
              <div data-original-title="" title="">
                <div class="_register-description" data-original-title="" title="">
                  Are you a student looking for higher education in Canada or the USA? Register today and let our team of experts guide you through your journey.
                </div>
                <div class="_login-register-btn" data-original-title="" title="">
                  <a href="https://www.applyboard.com/register" data-original-title="" title="">Student Registration</a>
                  <i class="fa fa-arrow-circle-right" data-original-title="" title=""></i>
                </div>
              </div>
            </div>
            <div class="_register-wrapper" data-original-title="" title="">
              <div class="_register-title" data-original-title="" title="">Agent</div>
              <div data-original-title="" title="">
                <div class="_register-description" data-original-title="" title="">
                  Do you recruit prospective students wanting to study in Canada or the USA? Register today and become an ApplyBoard Certified agent.
                </div>
                <div class="_login-register-btn" data-original-title="" title="">
                  <a href="https://www.applyboard.com/new_associate" data-original-title="" title="">Agent Registration</a>
                  <i class="fa fa-arrow-circle-right" data-original-title="" title=""></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



      
    </div>
    <!-- Footer -->
    <div id="footer" class="application-footer footer-large" data-original-title="" title="">
      <div class="container" data-original-title="" title="">
        <noscript data-original-title="" title="">
  <div id="nojs_warning" style="position: fixed; left: 0; top: 50px; padding: 10px; background: #d50000; z-index: 1030; margin: 0 auto; color: white; width: 100vw; text-align: center">
    <h3>You mush enable your javascript to use ApplyBoard Service!</h3><h3><a href="http://enable-javascript.com/" style="color: #90CAF9; text-decoration: underline">Click here</a> if you don't know how to enable javascript for your browser.</h3>
</div>

</noscript>
<div class="footer-widgets row th-widget-area" data-original-title="" title="">
  <div class="footer-area-1 col-md-3 col-sm-6" data-original-title="" title="">
    <section class="widget_text _widget widget_custom_html" data-original-title="" title="">
      <div class="widget_text widget-inner" data-original-title="" title="">
        <h3 class="widget-title" data-original-title="" title="">Quick Links</h3>
      </div>
      <div class="textwidget custom-html-widget" data-original-title="" title="">
        <ul data-original-title="" title="">
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/quick_search" data-original-title="" title="">Discover Programs</a>
          </li>
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/schools" data-original-title="" title="">Discover Schools</a>
          </li>
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/register" data-original-title="" title="">Sign In / Sign Up</a>
          </li>
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/forgot_password" data-original-title="" title="">Reset Password</a>
          </li>
        </ul>
      </div>
    </section>
    <section class="_widget widget-social" data-original-title="" title="">
      <div class="widget_text widget-inner" data-original-title="" title="">
        <h3 class="widget-title" data-original-title="" title="">Social</h3>
      </div>
      <div class="soc-widget" data-original-title="" title="">
        <a target="_blank" rel="noopener" href="https://www.facebook.com/applyboard" data-original-title="" title="">
          <i class="fa fa-facebook" data-original-title="" title=""></i>
        </a>
        <a target="_blank" rel="noopener" href="https://www.twitter.com/applyboard" data-original-title="" title="">
          <i class="fa fa-twitter" data-original-title="" title=""></i>
        </a>
        <a target="_blank" rel="noopener" href="https://www.instagram.com/applyboard" data-original-title="" title="">
          <i class="fa fa-instagram" data-original-title="" title=""></i>
        </a>
        <a target="_blank" rel="noopener" href="https://www.youtube.com/ApplyboardChannel" data-original-title="" title="">
          <i class="fa fa-youtube" data-original-title="" title=""></i>
        </a>
        <a target="_blank" rel="noopener" href="https://www.linkedin.com/company/applyboard" data-original-title="" title="">
          <i class="fa fa-linkedin" data-original-title="" title=""></i>
        </a>
      </div>
    </section>
  </div>
  <div class="footer-area-2 col-md-3 col-sm-6" data-original-title="" title="">
    <section class="widget_text _widget custom_html-5 widget_custom_html" data-original-title="" title="">
      <div class="widget_text widget-inner" data-original-title="" title="">
        <h3 class="widget-title" data-original-title="" title="">About</h3>
      </div>
      <div class="textwidget custom-html-widget" data-original-title="" title="">
        <ul data-original-title="" title="">
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/about_us" data-original-title="" title="">About US</a>
          </li>
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/testimonials" data-original-title="" title="">Testimonials</a>
          </li>
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/careers" data-original-title="" title="">Careers</a>
          </li>
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/blog" data-original-title="" title="">Blog</a>
          </li>
        </ul>
      </div>
    </section>
  </div>
  <div class="footer-area-3 col-md-3 col-sm-6" data-original-title="" title="">
    <section class="widget_text _widget custom_html-7 widget_custom_html" data-original-title="" title="">
      <div class="widget_text widget-inner" data-original-title="" title="">
        <h3 class="widget-title" data-original-title="" title="">Partners</h3>
      </div>
      <div class="textwidget custom-html-widget" data-original-title="" title="">
        <ul data-original-title="" title="">
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/partner_with_us" data-original-title="" title="">Partner with Us - Agents</a>
          </li>
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/certificates" data-original-title="" title="">School Certificates</a>
          </li>
        </ul>
      </div>
    </section>
    <section class="widget_text _widget custom_html-10 widget_custom_html" data-original-title="" title="">
      <div class="widget_text widget-inner" data-original-title="" title="">
        <h3 class="widget-title" data-original-title="" title="">FAQ</h3>
      </div>
      <div class="textwidget custom-html-widget" data-original-title="" title="">
        <ul data-original-title="" title="">
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/applyboard_fees" data-original-title="" title="">ApplyBoard Fees</a>
          </li>
          <li class="page_item" data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/faq_students" data-original-title="" title="">How ApplyBoard works</a>
          </li>
        </ul>
      </div>
    </section>
  </div>
  <div class="footer-area-4 col-md-3 col-sm-6" data-original-title="" title="">
    <section class="_widget widget-th-contact-info" data-original-title="" title="">
      <div class="widget-inner" data-original-title="" title="">
        <h3 class="widget-title" data-original-title="" title="">Contact Us</h3>
      </div>
      <div class="th-contact-info-widget" data-original-title="" title="">
        <div class="icon-block" data-original-title="" title="">
          <p data-original-title="" title="">
            <a target="_blank" rel="noopener" href="https://www.google.com/maps/place/101+Frederick+St+%23600,+Kitchener,+ON+N2H+6R3/@43.4513332,-80.4878482,17z/data=!3m1!4b1!4m5!3m4!1s0x882bf4ed285ffa17:0x88ec87e93e4f23d3!8m2!3d43.4513293!4d-80.4856595" data-original-title="" title="">
              <i class="fa fa-map-o" data-original-title="" title=""></i>
              <span data-original-title="" title="">Suite 600, 101 Frederick Street, Kitchener, ON. N2H 6R3, Canada.</span></a>
          </p>
        </div>
        <div class="icon-block" data-original-title="" title="">
          <p data-original-title="" title="">
            <a href="mailto:info@applyboard.com" data-original-title="" title="">
              <i class="fa fa-envelope-open-o" data-original-title="" title=""></i>
              <span data-original-title="" title="">info@applyboard.com</span></a>
          </p>
        </div>
        <div class="icon-block" data-original-title="" title="">
          <p data-original-title="" title="">
            <a target="_blank" rel="noopener" href="tel:519-900-6001" data-original-title="" title="">
              <i class="fa fa-mobile" data-original-title="" title=""></i>
              <span data-original-title="" title="">(519)-900-6001</span></a>
          </p>
        </div>
        <div class="icon-block" data-original-title="" title="">
          <p data-original-title="" title="">
            <i class="fa fa-clock-o" data-original-title="" title=""></i>
            <span data-original-title="" title="">Monday-Friday 9AM-5:30PM EST</span>
          </p>
        </div>
      </div>
    </section>
  </div>
</div>
<div class="footer-btm-bar" data-original-title="" title="">
  <div class="container" data-original-title="" title="">
    <div class="footer-copyright row" data-original-title="" title="">
      <div class="col-xs-12" data-original-title="" title="">
        <p data-original-title="" title="">
          <span class="footer_copy" data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/privacy-policy" data-original-title="" title="">Privacy Policy</a>
            |
            <a href="https://www.applyboard.com/ca/terms-of-service" data-original-title="" title="">Terms of Service</a>
            <br data-original-title="" title="">
            <a href="https://www.applyboard.com/ca/" data-original-title="" title="">© 2018 ApplyBoard.com</a>
          </span>
        </p>
      </div>
    </div>
  </div>
</div>

      </div>
</div>

<style data-original-title="" title="">


</style>


  </div>
  <!-- End page wrapper-->

</div>
<!-- End wrapper-->

<!-- Include javascript per-view -->
<!-- For demo purpose we include javascript in view but you can easily start SeedProject and organize it with Rails asset pipeline as you want -->






<div id="js_modals" data-original-title="" title="">
</div>



<script data-original-title="" title="">
$(function() {
  init_ui();
});
</script>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" style="position:fixed" data-original-title="" title="">
  <div class="slides" data-original-title="" title=""></div>
  <h3 class="title" data-original-title="" title=""></h3>
  <a class="prev" data-original-title="" title="">‹</a>
  <a class="next" data-original-title="" title="">›</a>
  <a class="close" data-original-title="" title="">×</a>
  <a class="play-pause" data-original-title="" title=""></a>
  <ol class="indicator" data-original-title="" title=""></ol>
</div>

  



</div></div><script type="application/javascript" src="./Login _ ApplyBoard_files/addstats"></script><script type="application/javascript" src="./Login _ ApplyBoard_files/web-rules"></script><script type="application/javascript" src="./Login _ ApplyBoard_files/email"></script><script type="application/javascript" src="./Login _ ApplyBoard_files/email(1)"></script><script type="application/javascript" src="./Login _ ApplyBoard_files/add-score"></script><script type="application/javascript" src="./Login _ ApplyBoard_files/add-score(1)"></script></body></html>